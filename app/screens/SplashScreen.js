import React, { Component } from 'react';
import { NavigationActions } from 'react-navigation';
import { DrawerNavigator } from 'react-navigation';
import {
  StyleSheet,
  View,
  Text,
  Image,
  Alert,
  TouchableNativeFeedback,
  ActivityIndicator,
  StatusBar
} from 'react-native';
import { color } from 'react-native-material-design-styles';

// Services
import Auth from '../services/Auth';

const AuthInstance = new Auth();

export default class SplashScreen extends Component {
  constructor(props){
    super(props);
    AuthInstance.isLogged(success=>{
      if(success)
        this.resetTo('Authorized'); 
      else
        this.resetTo('Unauthorized');
    })
  }
  render() {
    return (
      <View style={styles.container}>
        <StatusBar
            backgroundColor={color.paperOrange300.color}
            barStyle="light-content"
            hidden={true}
          />
        <View style={styles.content}>
          <Image style={styles.Image} source={require('../../resources/img/bill-ok.png')}/>
          <Text style={styles.title}>AprecioDePana</Text>
          <ActivityIndicator size="large" color="#FFF" />
        </View>
      </View>
    );
  }
  resetTo(route) {
    const actionToDispatch = NavigationActions.reset({
      index: 0,
      key: null,
      actions: [NavigationActions.navigate({ routeName: route })],
    });
    this.props.navigation.dispatch(actionToDispatch)
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    position: 'relative',
    backgroundColor: '#ffaf4b',
  },
  content: {
    flex: 1,
    position: 'absolute',
    top: 50,
    left: 28,
  },
  title: {
    textAlign: 'center',
    fontSize: 26,
    color: '#FFF',
    marginTop: 20,
    marginBottom: 30,
  },
  Image: {
    resizeMode: 'stretch',
    width: 280,
    height: 300,
    zIndex: 2,
  }
});
