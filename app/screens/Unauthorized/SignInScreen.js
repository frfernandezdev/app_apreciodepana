import React, { Component } from 'react';
import { DrawerNavigator,NavigationActions } from 'react-navigation';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TextInput,
  StatusBar,
  KeyboardAvoidingView
} from 'react-native';
import { 
  Button,
  SocialIcon, 
  FormLabel, 
  FormInput,
  FormValidationMessage
} from 'react-native-elements';
import { color } from 'react-native-material-design-styles';

// Components Custom

import {TextInputCustom} from '../../components';

// Services
import Auth from '../../services/Auth';

const AuthInstance = new Auth();

export default class SignInScreen extends Component {
  constructor(props){
    super(props);
    this.state = {};
  }
  render() {
    const { navigate } = this.props.navigation;
    return (
      <KeyboardAvoidingView
        style={styles.container}
        behavior="padding"
        resetScrollToCoords ={{x :  0 , y :  0 }}
        contentContainerStyle ={styles.container}
        scrollEnabled ={false}
      >
        <StatusBar
          backgroundColor={color.paperOrange300.color}
          barStyle="light-content"
          hidden={true}
        />
        <View style={styles.contentBack}>
          <Button
            raised
            borderRadius={50}
            icon={{name: 'navigate-before',size: 30}}
            title=""
            underlayColor="#f3a646"
            buttonStyle={styles.btnBack}
            onPress={() => navigate('Welcome')}
          />
        </View>
        <View style={styles.contentOmit}>
          <Button
            raised
            borderRadius={50}
            title="Omitir"
            underlayColor="#f3a646"
            buttonStyle={styles.btnOmit}
            onPress={() => navigate('Home')}
          />
        </View>
        <Image style={styles.Image} source={require('../../../resources/img/bill-ok.png')}/>
        <Text style={styles.title}>Iniciar Sesión</Text>
        <View style={styles.formLogin}>
          <TextInputCustom
            borderColor='#FFF'
            placeholderTextColor='#FFF'
            keyboardType='email-address'
            placeholder="Correo Electronico"
            color="#FFF"
            params={params => this.setState({mail: params})}
          />
          <TextInputCustom
            borderColor='#FFF'
            placeholderTextColor='#FFF'
            placeholder="Contraseña"
            color="#FFF"
            segurity={true}
            params={params => this.setState({password: params})}
          />
          <View style={{marginTop: 10}}>
            <Button 
              large 
              raised
              title="Iniciar Sesión"
              underlayColor="#f3a646"
              buttonStyle={styles.Button}
              onPress={() => this.OnSubmit()}
            />
          </View>
        </View>
      </KeyboardAvoidingView>
    );
  }
  OnSubmit() {
    console.log(this.state)
  //   if(this.state) {
  //     if(this.state.errorEmail===undefined && this.state.errorPassword===undefined)
  //       console.log(this.state)
  //       this.props.navigation('Splash') 
  //       // AuthInstance.SignIn(this.state)
  //         // .then(success => {
  //         //   if(success){
  //         //   }
  //         // })
  //   }else {

  //   }
  
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: color.paperOrange300.color,
  },
  Image: {
    resizeMode: 'stretch',
    width: 220,
    height: 220,
    zIndex: 2,
    marginTop: 16
  },
  title: {
    textAlign: 'center',
    fontSize: 26,
    color: '#FFF',
    marginTop: 16,
    marginBottom: 16,
  },
  contentSocial: {
    flex: 1,
    flexDirection: 'row',
    marginTop: 10
  },
  SocialIcons: {
    width: 50,
    height: 50
  },
  Button: {
    backgroundColor: color.paperOrange300.color,
    height: 50,
    borderWidth: 0.5,
    borderColor: '#FFF',
  },
  ButtonView: {
    width: 240,
    marginTop: 10
  },
  btnOmit: {
    backgroundColor: color.paperOrange300.color,
    borderColor: 'transparent'
  },
  contentOmit: {
    flex: 1,
    position: 'absolute',
    right: 0,
    top: 12,
    zIndex: 3,
  },
  btnBack: {
    width: 50,
    height: 50,
    backgroundColor: color.paperOrange300.color,
    borderColor: 'transparent'
  },
  contentBack: {
    flex: 1,
    position: 'absolute',
    left: 0,
    top: 12,
    zIndex: 3,
  },
  formLogin: {
    width: 240,
  }
})