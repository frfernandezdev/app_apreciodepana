import React, { Component } from 'react';
import { DrawerNavigator } from 'react-navigation';
import {
  StyleSheet,
  View,
  Text,
  Image,
  Alert,
  StatusBar,
  TouchableNativeFeedback,
} from 'react-native';
import { color } from 'react-native-material-design-styles';

export default class SignUpScreen extends Component {
  constructor(props){
    super(props);
  }
  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor={color.paperOrange300.color}
          barStyle="light-content"
          hidden={true}
        />
        <View style={styles.content}>
          <Image style={styles.Image} source={require('../../../resources/img/bill-ok.png')}/>
          <Text style={styles.title}>AprecioDePana Movil</Text>
          <Text style={styles.Loading}>SignUp</Text>
        </View>
      </View>
    );
  }
  OnAuth() {
    this.props.navigation.navigate('Home');
    console.log('OnPress')
    
    // AsyncStorage.getItem('@app:session')
    //  .then(token => {
    //    console.log(token);
    //  })
    // AsyncStorage.setItem(
    //   '@app:session', '1a45b5t9',
    //   error => {
    //     console.log(error)
    //    if(!error)
    //   }
    // );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    position: 'relative',
    backgroundColor: '#ffaf4b',
  },
  content: {
    flex: 1,
    position: 'absolute',
    top: 50,
    left: 28,
  },
  title: {
    textAlign: 'center',
    fontSize: 26,
    color: '#FFF',
    marginTop: 20,
    marginBottom: 30,
  },
  Loading: {
    color: '#FFF',
    textAlign: 'center',
  },
  Image: {
    resizeMode: 'stretch',
    width: 280,
    height: 300,
    zIndex: 2,
  }
});
