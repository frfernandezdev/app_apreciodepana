import PropTypes from 'prop-types';
import React, { Component } from 'react';
import {
  View,
  Text as NativeText,
  Image,
  Platform,
  StyleSheet,
  TouchableOpacity,
  TouchableHighlight,
  TouchableNativeFeedback,
  TouchableWithoutFeedback,
} from 'react-native';
import {  
  FormLabel, 
  FormInput,
  FormValidationMessage
} from 'react-native-elements';
import { color } from 'react-native-material-design-styles';

const Regex = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

class TextInputCustom extends Component {
  constructor(props){
    super(props);
    this.state = {};
    this.state.borderColor = this.props.borderColor ? this.props.borderColor: '#FFF';
    this.state.color = this.props.color ? this.props.color: '#FFF';
  }
  render(){
    const {
      placeholderTextColor,
      keyboardType,
      placeholder,
      color,
      segurity,
      type,
      ...attributes
    } = this.props;
    return (
      <View style={stylesForm.inputGroup}>
        <FormInput 
          containerStyle={
            [
              stylesForm.FormInputContainer,
              {
                borderColor: this.props.borderColor
              }
            ]
          }
          inputStyle={
            [
              stylesForm.FormInput, 
              {
                color: this.state.color
              }
            ]
          }
          placeholderTextColor={placeholderTextColor}
          {...attributes}
          placeholder={placeholder}
          secureTextEntry={segurity ? segurity: false}
          underlineColorAndroid="transparent"
          onChangeText={params => this.setState({params})}
          onEndEditing={() => this.OnValid(keyboardType ? keyboardType: segurity)}
          onBlur={() => this.OnBlur()}
        />
        <FormValidationMessage>{this.state.MsjError}</FormValidationMessage>
      </View>
    )
  }
  OnBlur() {
    console.log('Blur');
  }
  OnValid(type = false){
    switch(type){
      case 'email-address':
        this.__EmailValid();
        break; 
      case true:
      this.__PasswordValid();
        break;
    }
  }
  __EmailValid(){
    if(Regex.test(this.state.params)==false){
      this.setState({MsjError: 'Correo Electronico Invalido'});
      this.setState(
        {
          borderColor: 'red',
          color: 'red'
        }
      )
      this.props.params(false)   
    }else {
      delete this.state.MsjError;
      this.setState(this.state)
      this.setState(
        {
          borderColor: this.props.borderColor,
          color: this.props.color
        }
      )
      this.props.params(this.state.params)
    }
  }
  __PasswordValid(){
    if(this.props.segurity){
      if(this.state.params!=undefined)
        if(!this.state.params.length>0){
          this.setState({MsjError: 'Ingresa una Contraseña'});
          this.setState(
            {
              borderColor: 'red',
              color: 'red'
            }
          )
          this.props.params(false)
        }else {
          delete this.state.MsjError;
          this.setState(this.state)
          this.setState(
            {
              borderColor: this.props.borderColor,
              color: this.props.color
            }
          )
          this.props.params(this.state.params)
        }
    }
  }
}

TextInputCustom.propTypes = {
  borderColor: PropTypes.string,
  placeholderTextColor: PropTypes.string,
  keyboardType: PropTypes.string,
  placeholder: PropTypes.string,
  color: PropTypes.string,
  segurity: PropTypes.bool,
};

const stylesForm = StyleSheet.create({
  FormInputContainer: {
    borderBottomWidth: 1
  },
  FormInput: {
    
  }
})

export default TextInputCustom;